/* 
index.js
*/
$(document).ready(function () {
    console.log("Welcome to MSO");
    var gitHubRepoDiv = $("#gitHubRepos");

    function displayRepos(){
        console.log("Displaying Repositories");
        var gitHubQuery = "https://api.github.com/users/alperunal92/repos";

        $.get(gitHubQuery).done(function (r) {
            updateHtml(r);
        }).fail(function (err) {
            console.log("Failed to query Github");
        })
    }
    
    function updateHtml(repos) {
        gitHubRepoDiv.empty();

        $.each(repos, function (i, item) {
            var newResult = $("<div class='result'>" +
                "<div><a class=\"title\" href=\"" + item.html_url + "\">" + item.name + "</a></div>" +
                "<div>Language: " + item.language + "</div>" +
                "</div>");

            newResult.hover(function () {
                $(this).css("background-color", "lightgray");
            }, function () {
                $(this).css("background-color", "transparent");
            });

            gitHubRepoDiv.append(newResult);
        });
    }

    var showRepoButton = $("#showRepo");
    showRepoButton.on("click", function () {
        gitHubRepoDiv.toggle(500);

        if (showRepoButton.text() == "Hide Repos"){    
            gitHubRepoDiv.empty();
            showRepoButton.text("Show Github Repos");
        }else{
            displayRepos();
            showRepoButton.text("Hide Repos");
        }
    });

});
